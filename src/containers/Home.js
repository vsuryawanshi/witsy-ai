import React, { Component } from "react";
import Header from "../common/Header";
import axios from "axios";

const BGS = [
    require("../images/bg1.jpg"),
    require("../images/bg2.jpg"),
    require("../images/bg3.jpg"),
    require("../images/bg4.png"),
];

const HEADLINES = [
    "Supercharge your conversions with Image AI from DeepPulse",
    "Transform your listings into intelligent recommendations",
    "Bring Storytelling to your existing Image Product"
];

const EXPLORE = [
    {
        name:"Image Classification",
        imgUrl:"https://a0.muscache.com/im/pictures/8b7519ec-2c82-4c09-8233-fd4d2715bbf9.jpg?aki_policy=small"
    },
    {
        name:"Object Detection",
        imgUrl:"https://a0.muscache.com/im/pictures/cb8b3101-d419-4c17-8e2f-4989b39b98c3.jpg?aki_policy=small"
    },
    {
        name:"Image Classification",
        imgUrl:"https://a0.muscache.com/im/pictures/8b7519ec-2c82-4c09-8233-fd4d2715bbf9.jpg?aki_policy=small"
    },
    {
        name:"Image Classification",
        imgUrl:"https://a0.muscache.com/im/pictures/8b7519ec-2c82-4c09-8233-fd4d2715bbf9.jpg?aki_policy=small"
    }
];

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentSearchTerm:"",
            inputFocused:false,
            searchResults:[],
            searchInProgress:false
        };
        this.typingTimer = null;
        this.doneTypingInterval = 500;
        this.scrollHandler = this.scrollHandler.bind(this);
    }

    componentDidMount() {
        this.mainTitle.innerHTML = HEADLINES[0];
        this.enableBackgroundAnimation();
        this.enableTextAnimation();

        document.body.addEventListener("mousewheel", this.scrollHandler);
        document.body.addEventListener("DOMMouseScroll", this.scrollHandler);
    }

    componentWillUnmount(){
        document.body.removeEventListener("mousewheel",this.scrollHandler);
        document.body.removeEventListener("DOMMouseScroll", this.scrollHandler);
        clearInterval(this.textAnimationInterval);
        clearInterval(this.animationInterval);
        clearTimeout(this.timeout);
        clearTimeout(this.timeout1);
    }

    scrollHandler(){
        var self = this;
        var scrollTop = Math.max(window.pageYOffset, document.documentElement.scrollTop, document.body.scrollTop)
        if(scrollTop > 100){
            self.headerElem.setBackground(true);
        } else {
            self.headerElem.setBackground(false);
        }
    }

    enableTextAnimation() {
        this.textCounter = 1;
        this.textAnimationInterval = setInterval(() => {
            this.mainTitle.style.opacity = 0;
            this.timeout = setTimeout(() => {
                if (this.textCounter < HEADLINES.length - 1) {
                    this.mainTitle.innerHTML = HEADLINES[this.textCounter++];
                } else {
                    this.mainTitle.innerHTML = HEADLINES[this.textCounter];
                    this.textCounter = 0;
                }
                this.mainTitle.style.opacity = 1;
            }, 900);
        }, 5000);
    }

    enableBackgroundAnimation() {
        this.counter = 1;
        this.animationInterval = setInterval(() => {
            this.topImage.style.opacity = 0;
            this.topImage.style.transform = "scale(1.2)";
            this.timeout1 = setTimeout(() => {
                if (this.counter < BGS.length - 1) {
                    this.topImage.src = BGS[this.counter++];
                } else {
                    this.topImage.src = BGS[this.counter];
                    this.topImage.style.transform = "scale(1)";
                    this.counter = 0;
                }
                this.topImage.style.opacity = 1;
            }, 900);
        }, 5000);
    }

    searchQuery(){
        this.setState({
            searchInProgress:true
        },()=>{
            axios({
                method:"GET",
                url:"http://62.210.93.54:8050/api/researchPaper/search?text="+this.state.currentSearchTerm
            }).then(response => {
                this.setState({
                    searchResults:response.data.suggestList,
                    searchInProgress:false
                })
            }).catch(err => {
                console.log(err);
                this.setState({
                    searchInProgress:false
                })
            })
        })
    }

    render() {
        return (
            <div className="home-wrapper">
                <Header ref={node => this.headerElem = node}/>
                <section className="intro sec" style={{height:window.innerHeight,position:"relative",zIndex:11}}>
                    <div className="mask"/>
                    <img className="top-img imgg" src={BGS[0]} ref={node => this.topImage = node}/>
                    <div className="inner-container flexible">
                        <div style={{width:"100%",marginTop:(this.state.inputFocused ? -350 : -50), transition:"all 500ms ease"}}>
                            <div className="sec-title">
                                <div className="t" ref={node => this.mainTitle = node}>
                                    Book unique homes and experiences all over the world.
                                </div>
                            </div>
                            <div className="form-field search">
                                <input 
                                    placeholder="Search for something" 
                                    type="text" 
                                    className="inp" 
                                    value={this.state.currentSearchTerm}
                                    onFocus={()=>{
                                        this.setState({inputFocused:true})
                                    }}
                                    onBlur={()=>{
                                        setTimeout(()=>{
                                            this.setState({inputFocused:false})
                                        },400);
                                    }}
                                    onChange={(event)=>{
                                        this.setState({
                                            currentSearchTerm:event.target.value
                                        },()=>{
                                            clearTimeout(this.typingTimer);
                                            this.typingTimer = setTimeout(()=>{
                                                this.searchQuery();
                                            }, this.doneTypingInterval);
                                            
                                        });
                                    }}/>
                                    {
                                        this.state.inputFocused ?
                                        <div className="autocomplete-wrap">
                                            {
                                                this.state.searchResults.length > 0 ?
                                                <div className="items">
                                                    {
                                                        this.state.searchResults.map((sr,index)=>{
                                                            return (
                                                                <div className="autocomplete-item" key={index} onClick={()=>{
                                                                    this.setState({
                                                                        currentSearchTerm:sr.title
                                                                    },()=>{
                                                                        this.props.history.push("pdf-v/"+sr.arxivId);
                                                                    })
                                                                }}>
                                                                {sr.title}
                                                                </div>
                                                            );
                                                        })
                                                    }
                                                </div>
                                                :
                                                <div className="no-item">
                                                    Start typing to see results here
                                                </div>
                                            }
                                        </div>
                                        :
                                        null
                                    }
                            </div> 
                        </div>  
                    </div>
                </section>

                <section className="sec explore">
                    <div className="inner-container">
                        <div className="sec-title">Explore Witsy.ai</div>
                        <div className="exp-list">
                            {
                                EXPLORE.map((exp,index)=>{
                                    return(
                                        <div className="exp-item" key={index}>
                                            <div className="img-bg" style={{backgroundImage:"url( " + exp.imgUrl + ")"}} />
                                            <div className="img-text">{exp.name}</div>
                                        </div>
                                    );
                                })
                            }
                        </div>
                    </div>
                </section>
            </div>
        );
    }
}