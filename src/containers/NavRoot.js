import React, {Component} from "react";
import { Switch, Route } from "react-router-dom";

import Home from "./Home";
import Notfound from "./NotFound";
import PdfView from "./PdfView";

export default class NavRoot extends Component {
    constructor(props){
        super(props);
    }

    render(){
        return(
            <main>
                <Switch>
                    <Route exact path="/" component={Home}/>
                    <Route exact path="/pdf-v/:pdfid" component={PdfView}/>
                    <Route path="*" component={Notfound} />
                </Switch>
            </main>
        );
    }
}