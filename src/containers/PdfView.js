import React, {Component} from "react";
import axios from "axios";
import BlueHeader from "../common/BlueHeader";
import moment from "moment";

export default class PdfView extends Component{
    constructor(props){
        super(props);
        this.state = {
            apiCallInProgress:false,
            pdfData:null
        };
    }

    componentDidMount(){
        this.getPdfData(this.props.match.params.pdfid);
    }

    getPdfData(pdfid){
        axios({
            method:"GET",
            url:"http://62.210.93.54:8050/api/researchPaper?docId="+pdfid
        }).then(response => {
            this.setState({
                pdfData:response.data.results[0]
            })
        }).catch(err => {
            console.log(err);
        })
    }
    getFormattedDate(y,m,d){
        var date = new Date(y,m,d);
        return new moment(date).format("MMM DD, YYYY");
    }

    render(){
        return(
            <div className="pdf-view-wrap">
                <BlueHeader/>   
                <aside className="pdf-overview">
                    <div className="name-card">
                        <div className="pdfimg"/>

                        <div className="rw">
                            <div className="sech">Authors:</div>
                            <div className="secc">{
                                this.state.pdfData != null ? this.state.pdfData.authors.join(", ") : "NA"
                            }</div>
                        </div>
                    </div>   
                </aside>   
                <main className="pcontent">
                {
                    this.state.pdfData != null ?
                    <div className="pcard">
                        <img src={require("../images/ribbon.svg")} className="ribbon"/>
                        <div className="ptitle">{this.state.pdfData.title}</div>
                        <div className="pdate">{this.getFormattedDate(this.state.pdfData.yyyy, this.state.pdfData.mm, this.state.pdfData.dd)}</div>
                        <div className="ptags">
                            {
                                this.state.pdfData.subjects.map((tag,index)=>{
                                    return(
                                        <div className={`ptag` + (index % 2 == 0 ? " e" : " o")} key={index}>{tag}</div>
                                    )
                                })
                            }
                        </div>
                        <div className="ptext">{this.state.pdfData.description}</div>
                    </div>
                    :
                    null
                }
                </main> 
            </div>
        );
    }
}