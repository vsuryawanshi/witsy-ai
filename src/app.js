import React from "react";
import ReactDOM from "react-dom";
import "./styles/main.scss";
import "./images/favicon.ico";
import { BrowserRouter } from 'react-router-dom'

import NavRoot from "./containers/NavRoot";

ReactDOM.render(
<BrowserRouter>
<NavRoot/>
</BrowserRouter>, document.getElementById("root"));